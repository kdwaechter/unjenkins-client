/**
 * Created by Kurt on 3/16/16.
 */
var socket = new WebSocket("ws://kdzmkz-aq4z042k.cloudapp.net:8989/subscribe/job");
registerThemeLinks();
socket.onmessage = function (event) {
    var text = JSON.parse(event.data);
    while (document.getElementById('rest-output').firstChild) {
        document.getElementById('rest-output').removeChild(document.getElementById('rest-output').firstChild)
    }
    document.getElementById('rest-output').appendChild(makeUL(text));
}

socket.onclose = function (event) {
    var message = event.data;
    console.log("Closing connection")
    console.log(message);
};

socket.onerror = function(error) {
    console.log('WebSocket Error: ' + error);
};


function sendQuery(query){
    var obj = new Object();
    obj.event = "query";
    obj.userEventType = "query";
    obj.values = [query];

    var jsonString= JSON.stringify(obj);
    socket.send(jsonString);
}



function switchTheme(){
    var text = this.value;
    sendQuery(text);
}

function registerThemeLinks() {
            var theme = document.getElementById("searchbox");
            theme.addEventListener("keyup", switchTheme, false);


}

function makeUL(array) {
    // Create the list element:
    var list = document.createElement('ul');

    for(var i = 0; i < array.length; i++) {
        // Create the list item:
        var item = document.createElement('li');

        // Set its contents:
        item.appendChild(document.createTextNode(array[i]));

        // Add it to the list:
        list.appendChild(item);
    }

    // Finally, return the constructed list:
    return list;
}

// Add the contents of options[0] to #foo:
